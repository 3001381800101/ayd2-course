
# Proyecto - SoundStream

## Descripción del proyecto
SoundStream es una plataforma de streaming de música, intentando promover el arte Guatemalteco prometiendo una experiencia robusta, confiable y amigable para sus clientes. Es una plataforma completamente en la nube, diseñada para ser utilizada en cualquier navegador Web. En esta plataforma, los clientes podrán crear una cuenta y decidir suscribirse a un plan gratis o pagado. La diferencia entre ambos planes es únicamente la cantidad de canciones que un cliente puede escuchar seguido antes de recibir un anuncio, y la capacidad de retroceder canciones o reproducir playlists personales. La descripción de las funcionalidades que tendrá SoundStream será definida en el enunciado a continuación. 

# Fase 3

Esta fase consistirá en agregar nuevas funcionalidades a la aplicación web, y se realizará la configuración de CI/CD necesaria para llevar a cabo futuros cambios al proyecto.

## On Repeat
En esta vista se mostrarán las canciones que el usuario ha reproducido más veces, así como el historial de reproducción, on-repeat permitirá reproducir las canciones que están en ella. 

> :warning: Esta funcionalidad es únicamente para usuarios premium

## SoundStream stats of the month
Esta vista permitirá al usuario visualizar estadísticas de su cuenta para que puedan compartir en sus redes sociales.

Las estadísticas a tomar en cuenta son: 
- Canciones más reproducidas
- Artistas más reproducidos
- Álbumes más reproducidos
- Minutos de reproducción (obtenidos desde el historial de reproducción)
- Artista con más minutos de reproducción o álbumes con más minutos de reproducción
- Canción favorita

Por último, se debe crear una lista de reproducción con las 5 canciones favoritas (las canciones más reproducidas) del usuario.

> :warning: Esta funcionalidad es únicamente para usuarios premium

## Canciones favoritas
El usuario debe ser capaz de marcar sus canciones como "favorita". Esto permitirá al usuario escuchar una lista de reproducción por defecto con las canciones que haya marcado de esta manera.

# GITLAB CI-CD

![Diagrama del pipeline](./img/pipeline.drawio.png)

Configurar un pipeline de Gitlab (uno por repositorio que estén manteniendo) para realizar los siguientes trabajos (en este orden): 

1. Security audit
2. Lint & Prettier check
3. Unit Tests & Validación de coverage > 70% 
4. E2E tests (únicamente para frontend)
5. Build (únicamente para frontend)
6. Container push & tag
7. Deploy (App engine para frontend, GKE para backend)

Deberán configurar este pipeline para cada commit en la rama develop y master. 

> Ninguna otra rama debe de crear estos pipelines cuando se realicen.

- https://www.youtube.com/watch?v=7q9Y1Cv-ib0&ab_channel=GitLab
- https://docs.gitlab.com/ee/ci/quick_start/index.html
- https://www.youtube.com/watch?v=sIegJaLy2ug&ab_channel=GitLabUnfiltered
- https://www.youtube.com/watch?v=ZVUbmVac-m8&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=28&ab_channel=GitLabUnfiltered
- https://www.youtube.com/watch?v=Cn0rzND-Yjw&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=11&ab_channel=GitLabUnfiltered

## GITLAB RUNNERS

Configurarán un runner como una instancia de VM en GCE. Esto con el objetivo de utilizar un runner propio y no incurrir en gastos en la plataforma de Gitlab.

- https://www.youtube.com/watch?v=lDMAuTux-Tw&ab_channel=GitLabUnfiltered
- https://docs.gitlab.com/ee/install/google_cloud_platform/
- https://docs.gitlab.com/runner/

# E2E Testing

Utilizarán Selenium o Cypress para realizar pruebas end-to-end, simulando la interacción del usuario con el UI. 
Los escenarios a probar son: 

1. Ingresar a página de suscripciones, seleccionar suscripción premium, registrar al usuario, validar que llegó hasta la página de radio
2. Login del usuario con credenciales válidas, hasta la página de modificación de datos (y modificar algún dato)
3. Ingresar a página de radio del usuario, agregar canción a favoritos, ir a página de favoritos y comprobar que la canción esté agregada.
4. Ingresar a página de radio, cambiar canciones tres veces, ir a página de historial de canciones, comprobar que las tres canciones estén ahí.

![Diagrama E2E](img/ayd2_diagrama-E2E%20Tests.drawio.png)

## Consideraciones

- Utilizar los lenguajes, frameworks y herramientas detalladas en el enunciado.
- Se trabajará en equipos no mayores a 5 personas.
- Copias totales o parciales tendrán una nota de 0 puntos y será reportado a las autoridades correspondientes.
- Deben realizar al menos 2 sprints en cada fase. 
- Se revisará que hayan utilizado Slack para la comunicación de su equipo.
- Se revisará que hayan creado correctamente las historias y tickets en Jira. 
- Se revisará que hayan trabajado correctamente con Git-Flow.
- Todas las dudas se resolverán en el foro correspondiente en UEDi o en el periodo de clase. 

## Fechas de entrega

- PRIMERA FASE: **13 de junio de 2022**
- SEGUNDA FASE: **21 de junio de 2022**
- TERCERA FASE: **30 de junio de 2022**
