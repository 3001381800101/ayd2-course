import { 
  LOGIN_SUCCESS,
  LOGOUT
} from '../actions/types'

const initialState = {
  token: localStorage.getItem('auth-token'),
  autenticado: null,
  loading: true,
  user: null
}

export default function (state = initialState, action) {
  const { type, payload } = action
  switch(type){
    case LOGIN_SUCCESS: 
      localStorage.setItem('auth-token', payload.token)
      return {
        ...state,
        ...payload,
        autenticado: true,
        loading: false
      }

    case LOGOUT:
      localStorage.removeItem('auth-token')
      return {
        ...state,
        ...payload,
        token: null,
        autenticado: false,
        loading: false
      }
    default:
      return state
  }
}