import {
  ERROR,
  SEND_MESSAGE
} from '../actions/types'

const initialState = {
  messages: [],
  loading: true,
  error: ''
}

export default function (state = initialState, action) {
  console.log('Reducing state...', state)
  const { type, payload } = action
  switch (type) {
    case SEND_MESSAGE:
      console.log('Updating messages...', state?.messages, payload)
      const messages = state?.messages ?? []
      return {
        ...state,
        messages: [...messages, payload],
        loading: false,
        error: ''
      }

    case ERROR:
      return {
        ...state,
        messages: [{ message: 'Error en la API :(', id: '1' }],
        loading: false,
        error: payload
      }
    default:
      return state
  }
}