import { configureStore } from '@reduxjs/toolkit'
import rootReducer from './reducers'

const preloadedState = {}

const store = configureStore({
  preloadedState,
  reducer: rootReducer
})

export default store