import './App.css';

import { Provider } from 'react-redux'
import { useEffect } from 'react'
import store from './store'

import { loadUser } from './actions/auth'

import Login from './components/auth/Login'
import Messages from './components/messages/Messages'
import Logout from './components/auth/Logout';

function App() {

  useEffect(() => {
    store.dispatch(loadUser())
  }, [])

  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <Login />
          <Logout />
          <Messages />
        </header>
      </div>
    </Provider>
  );
}

export default App;
