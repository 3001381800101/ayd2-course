import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { logout } from '../../actions/auth'

import { Button } from '@mui/material'

const Logout = ({ logout, autenticado }) => {

  const onSubmit = async e => {
    e.preventDefault()
    logout()
  }

  if (!autenticado) {
    return null
  }

  return (
    <>
      <Button onClick={onSubmit} variant="outlined">Cerrar sesion</Button>
      <br></br>
    </>
  )
}

Logout.propTypes = {
  logout: PropTypes.func.isRequired,
  autenticado: PropTypes.bool
}

const mapStateToProps = state => ({
  autenticado: state.auth.autenticado
})

export default connect(mapStateToProps, { logout })(Logout)