import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import { connect } from 'react-redux'
import { login } from '../../actions/auth'

import { Button, TextField } from '@mui/material'

const Login = ({ login, autenticado }) => {

  const [userData, setUserData] = useState({
    "email": "leoag2@gmail.com",
    "password": "pwd1234" 
  })

  const onChange = e => setUserData({ ...userData, [e.target.name]: e.target.value })

  const onSubmit = async e => {
    e.preventDefault()
    login(email, password)
  }

  const sendMessage = async () => {
    try {
      const Message = {
        id: '123456',
        message: 'Hola desde react'
      }

      const { data } = await axios.post('/messages/create', { ...Message })
      console.log(`Resultado de crear libro: `, data)
    }
    catch (error) {
      setUserData({ ...userData, alertas: error.code })
      console.log(`Error haciendo la peticion de guardar mensaje: `, error)
    }
  }

  const { email, password } = userData


  if (autenticado) {
    return null
  }

  return (
    <Fragment>
      <TextField
        variant="outlined"
        type="email"
        label='Ingresa tu correo...'
        value={email}
        onChange={e => onChange(e)}
        name="email"
        required
      />
      <br />
      <TextField
        type="password"
        label='Ingresa tu password'
        value={password}
        onChange={e => onChange(e)}
        name="password"
        required
      />
      < br />
      <Button onClick={onSubmit} variant="outlined">Iniciar sesion</Button>
    </Fragment>
  )
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  autenticado: PropTypes.bool
}

const mapStateToProps = state => ({
  autenticado: state.auth.autenticado
})

export default connect(mapStateToProps, { login })(Login)