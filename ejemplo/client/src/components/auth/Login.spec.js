import { render, screen } from '@testing-library/react'
import Login from './Auth'

// 
jest.mock('axios', () => {
  return {
    post: () => {
      console.log('posting...')
      return jest.fn()
    }
  }
})


test('Render Login component', () => {
  render(<Login yaEstoyAutenticado={true} />)
  expect(screen.getByText('Registrate o inicia sesion')).not.toBeInTheDocument()
})


test('Falla si la llamada a la api retorna 400', () => {
  render(<Login />)
  expect(screen)
})