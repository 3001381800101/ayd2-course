import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'

import {
  Timeline,
  TimelineItem,
  TimelineSeparator,
  TimelineConnector,
  TimelineContent,
  TimelineDot
} from '@mui/lab'

const Show = ({ messages }) => {

  console.log('Rendering messages: ', messages )
  if (!messages || !messages.length) {
    return null
  }

  const createMessage = ({message, id, user}) => (
    <TimelineItem key={id}>
      <TimelineSeparator>
        <TimelineDot></TimelineDot>
        <TimelineConnector></TimelineConnector>
      </TimelineSeparator>
      <TimelineContent>{message}</TimelineContent>
    </TimelineItem>
  )

  return (
    <Timeline position='alternate'>
      {messages.map(createMessage)}
    </Timeline>
  )
}

Show.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
  }))
}

export default Show