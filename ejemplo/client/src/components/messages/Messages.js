import Send from './Send'
import Show from './Show'
import { Grid, Card, CardContent, Typography } from '@mui/material'
import { connect } from 'react-redux'

const Messages = ({ autenticado, messages }) => {
  if (!autenticado){
    return null
  }
  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <Card sx={{ minWidth: 275, marginLeft: '25px' }}>
          <CardContent>
            <Typography sx={{ fontSize: 18 }} color="text.secondary" gutterBottom>
              Cloud - ¿En qué estás pensando?
            </Typography>
            <Send />
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card sx={{ minWidth: 275, marginLeft: '25px', marginRight: '25px' }}>
          <CardContent>
            <Typography sx={{ fontSize: 18 }} color="text.secondary" gutterBottom>
              Mensajes enviados
            </Typography>
            <Show messages={messages} />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

Messages.propTypes = {
}

const mapStateToProps = state => ({
  autenticado: state.auth.autenticado,
  messages: state.message.messages
})

export default connect(mapStateToProps, {})(Messages)