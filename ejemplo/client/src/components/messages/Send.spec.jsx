import { render, fireEvent, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Send from './Send'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'
import axiosMock from 'axios'

// PRUEBAS UNITARIAS

jest.mock('axios')

afterEach(() => {
  jest.clearAllMocks()
})

test('Render Send component', () => {
  const mockStore = configureStore()
  const store = mockStore({})
  jest.spyOn(global.Math, 'random').mockReturnValue(0)
  const { getByText, queryByText } = render(
    <Provider store={store}>
      <Send />
    </Provider>
  )
  expect(getByText('Caracteres: ', { exact: false })).toBeInTheDocument()
  expect(getByText('Enviar')).toBeInTheDocument()
  expect(getByText('green prairie dog')).toBeInTheDocument()
  jest.spyOn(global.Math, 'random').mockRestore()
})

test('Type values', async () => {
  const mockStore = configureStore([thunk])
  const store = mockStore({})
  const { getByRole } = render(
    <Provider store={store}>
      <Send />
    </Provider>
  )
  const txtBox = getByRole('textbox')
  userEvent.clear(txtBox)
  userEvent.type(txtBox, 'Hello world!')

  expect(await screen.findByText('Hello world!')).toBeInTheDocument()

  const spy = jest.spyOn(axiosMock, 'post')
  axiosMock.post.mockResolvedValueOnce({ data: 'ok' })
  fireEvent.click(getByRole('button', { name: 'Enviar' }))
  
  const msgCall = axiosMock?.post?.mock?.calls[0][1]

  expect(spy).toHaveBeenCalled()
  expect(spy).toHaveBeenCalledWith('/messages/create', expect.anything())
  expect(msgCall?.message).toEqual('Hello world!')

  axiosMock.post.mockReset()
})

test('Render send component with extra validation', () => {
  const mockStore = configureStore()
  const store = mockStore({})
  jest.spyOn(global.Math, 'random').mockReturnValue(0.99)
  const { getByText } = render(
    <Provider store={store}>
      <Send />
    </Provider>
  )
  expect(getByText('Caracteres: ', { exact: false })).toBeInTheDocument()
  expect(getByText('Enviar')).toBeInTheDocument()
  expect(getByText('white cat and white cat')).toBeInTheDocument()
  jest.spyOn(global.Math, 'random').mockRestore()
})

test('Fire send event with OK status', async () => {
  const mockStore = configureStore([thunk])
  const store = mockStore({})

  const { getByRole } = render(
    <Provider store={store}>
      <Send />
    </Provider>
  )
  const spy = jest.spyOn(axiosMock, 'post')
  axiosMock.post.mockResolvedValueOnce({ data: 'ok' })
  fireEvent.click(getByRole('button', { name: 'Enviar' }))

  expect(spy).toHaveBeenCalled()
  expect(spy).toHaveBeenCalledWith('/messages/create', expect.anything())

  axiosMock.post.mockReset()
})

test('Fire send event with ERROR status', (next) => {
  const mockStore = configureStore([thunk])
  const store = mockStore({})

  const { getByRole } = render(
    <Provider store={store}>
      <Send />
    </Provider>
  )
  const spy = jest.spyOn(console, 'log').mockImplementation()
  axiosMock.post.mockRejectedValueOnce({ error: 'error' })
  fireEvent.click(getByRole('button', { name: 'Enviar' }))

  setTimeout(() => {
    expect(spy).toHaveBeenCalled()
    expect(console.log).toHaveBeenCalledWith('Error en la peticion', expect.anything())
    next()
  }, 500)
})
