import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { sendMessage } from '../../actions/message'
import { connect } from 'react-redux'

import { Button, TextareaAutosize } from '@mui/material'

const animal = [
  'prairie dog',
  'hedgehog',
  'bat',
  'porpoise',
  'zebra',
  'grizzly bear',
  'dog',
  'orangutan',
  'alpaca',
  'cat'
]

const color = [
  'green',
  'yellow',
  'red',
  'blue',
  'white'
]

const Send = ({ sendMessage }) => {
  const random = array => array[Math.floor(Math.random() * array.length)]
  const randomMessage = (validateLength = true) => {
    const ranM = `${random(color)} ${random(animal)}`
    if (ranM.length < 10 && validateLength){
      return ranM + ' and ' + randomMessage(false)
    }
    return ranM
  }
  const [message, setMessage] = useState(randomMessage())

  const onChange = e => setMessage(e.target.value)

  const onSubmit = async e => {
    e.preventDefault()
    sendMessage(message)
    setMessage(randomMessage())
  }

  return (
    <Fragment>
      <TextareaAutosize
        variant="outlined"
        value={message}
        onChange={e => onChange(e)}
        name="message"
        required
        style={{ width: '80%', maxWidth: '80%', height: '150px'}}
      />
      < br />
      <p style={{ fontSize: '12px', alignContent: 'end', textAlign: 'right', 'paddingRight': '40px'}}>Caracteres: { message.length }</p>
      <Button onClick={onSubmit} color="secondary" variant="contained" name="Enviar">Enviar</Button>
    </Fragment>
  )
}

Send.propTypes = {
  sendMessage: PropTypes.func.isRequired
}

const mapStateToProps = state => ({})

export default connect(mapStateToProps, { sendMessage })(Send)