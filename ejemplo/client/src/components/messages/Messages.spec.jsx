import { render, fireEvent, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Messages from './Messages'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'
import axiosMock from 'axios'
import { sendMessage } from '../../actions/message'

// PRUEBA DE INTEGRACION

jest.mock('axios')

afterAll(() => {
  jest.clearAllMocks()
})

test('Render Message component autenticated', () => {
  const mockStore = configureStore()
  const store = mockStore({
    auth: {
      autenticado: true
    },
    message: {
      messages: []
    }
  })
  jest.spyOn(global.Math, 'random').mockReturnValue(0)
  const { getByText } = render(
    <Provider store={store}>
      <Messages />
    </Provider>
  )
  expect(getByText('Caracteres: ', { exact: false })).toBeInTheDocument()
  expect(getByText('Enviar')).toBeInTheDocument()
  expect(getByText('green prairie dog')).toBeInTheDocument()
  expect(getByText('Mensajes enviados')).toBeInTheDocument()
  jest.spyOn(global.Math, 'random').mockRestore()
})

test('Dont Render Message component when unautenticated', () => {
  const mockStore = configureStore()
  const store = mockStore({
    auth: {
      autenticado: false
    },
    message: {
      messages: []
    }
  })
  jest.spyOn(global.Math, 'random').mockReturnValue(0)
  const { queryByText } = render(
    <Provider store={store}>
      <Messages />
    </Provider>
  )
  expect(queryByText('Caracteres: ')).not.toBeInTheDocument()
  expect(queryByText('Enviar')).not.toBeInTheDocument()
  expect(queryByText('green prairie dog')).not.toBeInTheDocument()
  expect(queryByText('Mensajes enviados')).not.toBeInTheDocument()
  jest.spyOn(global.Math, 'random').mockRestore()
})

test('Send message from Send component and display it in Show component', (next) => {
  const mockStore = configureStore([thunk])
  const store = mockStore({
    auth: {
      autenticado: true
    },
    message: {
      messages: []
    }
  })
  jest.spyOn(global.Math, 'random').mockReturnValue(0)

  const { getByRole, getByText } = render(
    <Provider store={store}>
      <Messages />
    </Provider>
  )
  const spy = jest.spyOn(axiosMock, 'post') // axios.post
  axiosMock.post.mockResolvedValueOnce({ data: { message: 'ok', id: '1234' } })
  fireEvent.click(getByRole('button', { name: 'Enviar' }))
  userEvent.clear(getByRole('textbox'))

  expect(spy).toHaveBeenCalledTimes(1)
  expect(spy).toHaveBeenCalledWith('/messages/create', expect.anything())

  store.dispatch(sendMessage())

  setTimeout(() => {
    //expect(getByText('Mensajes enviadsos')).toBeInTheDocument()
    next()
  }, 500)
})

