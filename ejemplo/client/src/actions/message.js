import axios from "axios"
import { v4 as uuid } from 'uuid'

import {
  SEND_MESSAGE,
  ERROR
} from './types'

export const sendMessage = (message) => async dispatch => {
  try {
    console.log('Enviando mensaje...', message)
    const { data: payload } = await axios.post('/messages/create', { message, id: uuid().substring(0, 10)})
    console.log('Dispatching message: ', payload)
    dispatch({
      type: SEND_MESSAGE,
      payload
    })
  }
  catch(error){
    console.log('Error en la peticion', error)
    dispatch({
      type: ERROR,
      payload: error?.response?.statusText ?? error?.message ?? error?.msg ?? 'Error en la llamada a la API'
    })
  }
}