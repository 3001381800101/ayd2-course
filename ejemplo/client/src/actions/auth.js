import axios from 'axios'
import setAuthToken from '../helpers/setAuthToken'
import { LOGIN_SUCCESS, LOGOUT, ERROR } from './types'

export const login = (email, password) => async dispatch => {
  try {
    console.log('Realizando login: ', email, password)
    const { data: payload } = await axios.post('/auth/login', { email, password })
    console.log('Token: ', payload.token)
    setAuthToken(payload.token)
    dispatch({
      type: LOGIN_SUCCESS,
      payload
    })
  }
  catch(error){
    console.log(`Error en la peticion: `, error)
    dispatch({
      type: ERROR,
      payload: error?.response?.statusText ?? error?.message ?? error?.msg ?? 'Error en la llamada a la API'
    })
  }
}

export const logout = () => async dispatch => {
  console.log(`Logging out...`)
  dispatch({
    type: LOGOUT,
    payload: {}
  })
}

export const loadUser = () => async dispatch => {
  const token = localStorage.getItem('auth-token')
  console.log(`Token cargado: `, token)
  if (token){
    setAuthToken(token)
    dispatch({
      type: LOGIN_SUCCESS,
      payload: { token }
    })
  }
}