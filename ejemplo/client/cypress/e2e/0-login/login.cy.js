

describe('Login page', () => {
  it('Loads successfully', () => {
    //arrange 
    cy.visit('http://localhost:3000')

    cy.get('input[name="email"]')
      .clear()
      .type('Hello@World.com')
    
    cy.get('input[name="password"]')
      .clear()
      .type('password')

    // act

    cy.get('button')
      .click()

    // assert
    cy.get('p')
      .contains('Cloud - ¿En qué estás pensando?')
    cy.get('p')
      .contains('Mensajes enviados')

  })

  it('save message', () => {
    cy.get('textarea[name="message"]')
      .clear()
      .type('mensajito12345')

    cy.get('button[name="Enviar"]')
      .click()

    cy.get('div')
      .contains('mensajito12345')

  })
})