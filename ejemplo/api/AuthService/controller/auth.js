const DbRepo = require("dbrepo");
const router = require('express').Router()
const { check, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const JWT_KEY = process.env.JWT_KEY

const db = new DbRepo()

router.post('/register', [
  check('email', 'Por favor envie su email').isEmail(),
  check('password', 'Por favor incluya su password').exists()
], async (req, res) => {

  console.log(errors.hola.mundo)
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() })
  }

  const { email, password } = req.body
  global.log.debug(`Registrando usuario con ${email}: ${password}`)
  const user = { id: email, password }
  const salt = await bcrypt.genSalt(10)
  user.password = await bcrypt.hash(password, salt)
  global.log.debug(`Pwd encriptado: ${user.password}`)

  const payload = {
    userId: user.id
  }

  global.log.debug(`Generando JWT ${JWT_KEY}`)

  jwt.sign(
    payload,
    JWT_KEY,
    { expiresIn: 10000 },
    async (err, token) => {
      global.log.debug(`Error: ${err}, token: ${token}`)
      if (err) {
        return res.status(500).send(err)
      }
      const result = await db.save(user)
      if (result.msg === 'ERROR') {
        return res.sendStatus(500)
      }
      return res.send({ token })
    }
  )
})

router.post('/login', [
  check('email', 'Por favor incluye un email valido').isEmail(),
  check('password', 'Por favor incluye un password').exists()
], async (req, res) => {

  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).send({ errors: errors.array() })
  }

  const { email, password } = req.body
  global.log.debug(`Iniciando sesion con ${email}: ${password}`)

  const sendInvalidCredentials = () => res.status(400).send({ errors: [{ msg: 'Credenciales invalidas' }] })

  const user = await db.get(email)
  if (!user) {
    return sendInvalidCredentials()
  }

  global.log.debug(`Encontrado usuario con el email: ${email}`)
  global.log.debug(user)
  global.log.debug(`Pass: ${password}, userPwd: ${user.password}`)
  const pwdMatch = await bcrypt.compare(password, user.password)
  if (!pwdMatch) {
    global.log.debug('Passwords invalidos')
    return sendInvalidCredentials()
  }

  const payload = {
    userId: user.id
  }

  jwt.sign(
    payload,
    JWT_KEY,
    { expiresIn: 10000 },
    (err, token) => {
      if (err) return res.status(500).send(err)

      return res.send({ token })
    }
  )
})

module.exports = router