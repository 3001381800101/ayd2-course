const morgan = require('morgan')
const cors = require('cors')
const helmet = require('helmet')
const express = require('express') //body-parser

const createCommonMiddleware = (app) => {
  app.use(cors())
  app.use(helmet())
  app.use(morgan('common', {}))
  app.use(express.json())
}

module.exports = createCommonMiddleware