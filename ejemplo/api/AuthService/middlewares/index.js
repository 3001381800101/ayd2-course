const createCommonMiddleware = require('./common')

const applyMiddleware = (app) => {
  createCommonMiddleware(app)
}

module.exports = applyMiddleware