require('./logger/logger')
const express = require('express')
const middleware = require('./middlewares')
// const swaggerUi = require('swagger-ui-express')
// const swaggerDocument = require('./swagger.json')

const AuthController = require('./controller/auth')

const createApp = () => {
  const app = express()
  middleware(app)
  app.use('/auth', AuthController)
  //app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

  return app
}

const app = createApp()
const PORT = process.env.PORT || 3000
app.listen(PORT, () => global.log.info(`API ejecutandose en el puerto ${PORT}`))

module.exports = createApp