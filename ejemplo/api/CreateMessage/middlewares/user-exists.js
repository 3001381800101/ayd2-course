
const DbRepo = require('dbrepo')
const db = new DbRepo()

module.exports = async (req, res, next) => {
  const id = req.userId
  if (!id){
    return res.status(401).send({ msg: 'Usuario no autenticado' })
  }

  const user = await db.get(id)
  delete user.password
  if (!user) {
    return res.status(401).send({ msg: 'Usuario no encontrado, vuelva a iniciar sesion' })
  }

  req.user = user
  next()
}