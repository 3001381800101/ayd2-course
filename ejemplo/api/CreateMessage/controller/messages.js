const router = require('express').Router()
const DbRepo = require('dbrepo')
const validator = require('../middlewares/validator')
const auth = require('../middlewares/auth')
const userExists = require('../middlewares/user-exists')

const db = new DbRepo()

const MessageController = {
  create: async (req, res) => {
    const { user, body: { message, id }, userId } = req
    global.log.debug(`> ${userId} dice: ${message}`)
    const data = { message, id, user } 
    await db.save(data)
    return res.status(201).send(data)
  }
}

module.exports = MessageController

router.post('/create', [auth, userExists, validator], MessageController.create)

router.get('/:id', [auth, userExists], async (req, res)=>{
  const id = req.params.id
  const message = await db.get(id)
  global.log.debug(`Mensaje: ${JSON.stringify(message)}`)
  if (!message){
    return res.status(404).send({ msg: 'El mensaje no existe' })
  }
  if (message?.user?.id !== req.userId){
    return res.status(403).send({ msg: 'No tienes permiso para ver este mensaje' })
  }

  return res.send(message)
})


module.exports = router