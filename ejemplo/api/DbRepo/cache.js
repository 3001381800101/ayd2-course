const redis = require('redis')

module.exports = class CacheService {
  constructor(){
    this.key = ''
    this.redisHost = process.env.REDIS_HOST || '127.0.0.1'
    this.redisPort = process.env.REDIS_PORT || 6379
    global.log.debug(`Conectando a redis: redis://${this.redisHost}:${this.redisPort}`)
    this.redisClient = redis.createClient({
      url: `redis://${this.redisHost}:${this.redisPort}`
    })
    this.redisClient.connect()
  }

  async get(id){
    try {
      const value = await this.redisClient.get(id)
      return value ? JSON.parse(value) : null
    }
    catch(error){
      console.log(error)
      global.log.error({ error, msg: `Error obteniendo llave ${id} desde redis`})
    }
  }

  async store(id, data){
    try {
      global.log.debug(`Guardando ${id}:  ${JSON.stringify({ data })}`)
      await this.redisClient.set(id, JSON.stringify({ data }))  
    }
    catch(error){
      console.log(error)
      global.log.error({ errorMessage: error.msg, msg: `Error guardando en redis ${id}`, data})
    }
  }
}