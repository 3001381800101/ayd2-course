
const Cache = require('./cache')

module.exports = class DbRepo {
  constructor(){
    this.cache = new Cache()
  }

  async save(data){
    try {
      //const id = await this.mysql.save(data)
      this.cache.store(data.id, data)
      return { msg: 'OK' }
    }
    catch(error){
      global.log.error({ error, msg: `Error guardando valor desde la bd ${data}`})
      return { msg: 'ERROR' }
    }
  }

  async get(id){
    const cachedValue = await this.cache.get(id)
    if (cachedValue){
      global.log.debug(`${cachedValue} retornado de Redis`)
      return cachedValue.data
    }
    global.log.info(`El ID: ${id} todavia no existe en REDIS`)
    return null
    //const result = this.mysql.select(id)
    //this.cache.store(id, result)
    //return result
  }

  async update(id, data){
    this.cache.remove(id)
    const result = this.mysql.update(id, data)
    return result
  }

  /**
   * UPDATE
   * DELETE
   * GET ALL
   * GET ONE
   * FIND ONE
   * CREATE
   * PATCH
   */
}